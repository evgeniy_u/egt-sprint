function Lane(number) {
	var laneOffset = number * 3.4;

	this.runLength = 400;
	this.straightLength = 84.4;
	this.turnRadius = 38.0165;
	this.laneWidth = 1.22;
	this.length = (2 * this.straightLength) + 2 * Math.PI * (this.turnRadius + (number) * this.laneWidth);
	this.startOffset = this.length - this.runLength + (number * -0.845) - 8; // Magic &) to align end position to finish line on backgound
	this.startOffsetPercent = this.startOffset / (this.length / 100);

	this.number = number;
	this.width = 410 + laneOffset * 2;
	this.height = 370 + laneOffset * 2;
	this.offsetX = 205 - laneOffset;
	this.offsetY = 70 - laneOffset;
	this.cornerRadius = 190 + laneOffset;

	// Top straight line
	this.section2 = {
		start: {
			x: this.offsetX + this.width,
			y: this.offsetY - number * 2
		},
		end: {
			x: this.offsetX,
			y: this.offsetY - number * 2
		}
	};

	// Bottom straight line
	this.section4 = {
		start: {
			x: this.offsetX,
			y: this.offsetY + this.height + number * 2
		},
		end: {
			x: this.offsetX + this.width,
			y: this.offsetY + this.height + number * 2
		}
	};

	// First turn
	this.section1 = {
		start: {
			x: this.section4.end.x,
			y: this.section4.end.y
		},
		end: {
			x: this.section2.start.x,
			y: this.section2.start.y
		},
		mid1: {
			x: this.section4.end.x + this.cornerRadius,
			y: this.section4.end.y - 28 - this.cornerRadius + this.height / 2
		},
		mid2: {
			x: this.section4.end.x + this.cornerRadius,
			y: this.section4.end.y + 28 - this.cornerRadius - this.height / 2
		}
	};

	// Last turn
	this.section3 = {
		start: {
			x: this.section2.end.x,
			y: this.section2.end.y
		},
		end: {
			x: this.section4.start.x,
			y: this.section4.start.y
		},
		mid1: {
			x: this.section2.end.x - this.cornerRadius,
			y: this.section2.end.y + 28 + this.cornerRadius - this.height / 2
		},
		mid2: {
			x: this.section2.end.x - this.cornerRadius,
			y: this.section2.end.y - 28 + this.cornerRadius + this.height / 2
		}
	};
}

Lane.prototype.getPosition = function(lapPercentCompleted) {
	var nextPosition, percent;

	if (lapPercentCompleted < 0) {
		lapPercentCompleted = 100 + lapPercentCompleted;
	}

	if (lapPercentCompleted < 25) {
		percent = lapPercentCompleted / 25;
		nextPosition = this.getCubicBezierXYatPercent(this.section1.start, this.section1.mid1, this.section1.mid2, this.section1.end, percent);
	} else if (lapPercentCompleted < 50) {
		percent = (lapPercentCompleted - 25) / 25;
		nextPosition = this.getLineXYatPercent(this.section2.start, this.section2.end, percent);
	} else if (lapPercentCompleted < 75) {
		percent = (lapPercentCompleted - 50) / 25;
		nextPosition = this.getCubicBezierXYatPercent(this.section3.start, this.section3.mid1, this.section3.mid2, this.section3.end, percent);
	} else if (lapPercentCompleted < 100) {
		percent = (lapPercentCompleted - 75) / 25;
		nextPosition = this.getLineXYatPercent(this.section4.start, this.section4.end, percent);
	}

	return nextPosition;
};

Lane.prototype.getLineXYatPercent = function(startPt, endPt, percent) {
	var dx = endPt.x - startPt.x;
	var dy = endPt.y - startPt.y;
	var X = startPt.x + dx * percent;
	var Y = startPt.y + dy * percent;
	return ({
		x: X,
		y: Y
	});
}

// cubic bezier percent is 0-1
Lane.prototype.getCubicBezierXYatPercent = function(startPt, controlPt1, controlPt2, endPt, percent) {
	var x = this.CubicN(percent, startPt.x, controlPt1.x, controlPt2.x, endPt.x);
	var y = this.CubicN(percent, startPt.y, controlPt1.y, controlPt2.y, endPt.y);
	return ({
		x: x,
		y: y
	});
};

// cubic helper formula at percent distance
Lane.prototype.CubicN = function(pct, a, b, c, d) {
	var t2 = pct * pct;
	var t3 = t2 * pct;
	return a + (-a * 3 + pct * (3 * a - a * pct)) * pct + (3 * b + pct * (-6 * b + b * 3 * pct)) * pct + (c * 3 - c * 3 * pct) * t2 + d * t3;
};