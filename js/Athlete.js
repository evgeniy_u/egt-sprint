function Athlete(stage, lane) {
	this.lane = new Lane(lane);
	this.icon = new PIXI.Graphics();
	this.finished = false;
	this.speed = 1;
	this.completedPercent = 0;
	this.completed = 0;
	this.color = Math.floor(Math.random() * 16777215);

	this.icon.lineStyle(1, 0xFFFFFF, 1);
	this.icon.beginFill(this.color);
	this.icon.drawCircle(0, 0, 3);
	this.icon.endFill();

	stage.addChild(this.icon);

	this.setPosition();
}

Athlete.prototype.update = function() {
	if (this.completed < this.lane.runLength) {
		this.completed += this.speed;
		this.completedPercent = this.completed / (this.lane.length / 100);
		this.setPosition();
	} else {
		this.finished = true;
	}
};

Athlete.prototype.setPosition = function() {
	var nextPosition = this.lane.getPosition(this.completedPercent + this.lane.startOffsetPercent);

	if (nextPosition) {
		this.icon.x = nextPosition.x;
		this.icon.y = nextPosition.y;
	}
};