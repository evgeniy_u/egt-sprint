(function() {
	var button = document.getElementById('start-button'),
		stage,
		renderer,
		currentSprint;

	PIXI.loader
		.add('images/stadium.png')
		.load(createSprint);

	button.addEventListener('click', handleButtonClick);

	function createSprint() {
		if (renderer) {
			document.body.removeChild(renderer.view);
		}

		stage = new PIXI.Container();
		renderer = PIXI.autoDetectRenderer(800, 500, {
			antialias: true,
			transparent: true,
			resolution: 1
		});
		var stadium = new PIXI.Sprite(PIXI.loader.resources['images/stadium.png'].texture);
		stage.addChild(stadium);

		currentSprint = new Sprint(renderer, stage, fixSprint(8), onSprintComplete);

		document.body.appendChild(renderer.view);
	}

	function fixSprint(athletesCount) {
		var athletes = [],
			minSpeed = 0.8,
			maxSpeed = 1,
			winnerFasterBy = maxSpeed / 100 * 1;

		for (var laneIndex = 0; laneIndex < athletesCount; laneIndex++) {
			athletes.push(new Athlete(stage, laneIndex));
		}

		// Atheletes now order in way that they should finish.
		fixAthletesPlaces(athletes);

		// Winner run at max speed.
		athletes[0].speed = maxSpeed;

		console.log('Winner will be on lane %d', athletes[0].lane.number + 1);

		// Set random speed for other athletes.
		for (var z = 1; z < athletesCount; z++) {
			athletes[z].speed = Math.random() * (maxSpeed - winnerFasterBy - minSpeed) + minSpeed;
		}

		return athletes;
	}

	function onSprintComplete() {
		button.innerHTML = 'Reset';
	}

	function handleButtonClick() {
		if (currentSprint.finished) {
			createSprint();
			button.innerHTML = 'Start';
		} else if (currentSprint.started) {
			button.innerHTML = 'Start';
			currentSprint.stop();
		} else {
			currentSprint.start();
			button.innerHTML = 'Stop';
		}
	}

	function fixAthletesPlaces(array) {
		for (var i = array.length - 1; i > 0; i--) {
			var j = Math.floor(Math.random() * (i + 1));
			var temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
	}
})();