function Sprint(renderer, stage, athletes, onSprintComplete) {
	this.athletes = athletes;
	this.started = false;
	this.finished = false;
	this.frameRequestId;
	this.renderer = renderer;
	this.stage = stage;
	this.onSprintComplete = onSprintComplete;

	this.render();
}

Sprint.prototype.start = function() {
	console.log('Sprint started');
	this.started = true;
	this.update();
};

Sprint.prototype.stop = function() {
	console.log('Sprint stopped');
	this.started = false;
	cancelAnimationFrame(this.frameRequestId);
};

Sprint.prototype.update = function() {
	var athletesFinished = 0;

	this.athletes.forEach(function(athlete) {
		if (athlete.finished) {
			athletesFinished += 1;
		} else {
			athlete.update();
		}
	});

	if (athletesFinished === this.athletes.length) {
		console.log('Sprint finished');
		this.finished = true;
		this.onSprintComplete();
	}

	if (this.started && !this.finished) {
		this.frameRequestId = requestAnimationFrame(this.update.bind(this));
	}

	this.render();
};

Sprint.prototype.render = function() {
	this.renderer.render(this.stage);
};
